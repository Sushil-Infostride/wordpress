<?php
/* 
theme function file
 */
// function () {
//     mentor_theme_setup
//     add_theme_support('custom-logo');

//     add_theme_support('title-tag');

//     add_theme_support('post-thumbnails');

//     add_image_size ('home-featured', 680, 400, array('center', 'center'));
//     add_image_size ('single-post', 580, 272, array('center', 'center'));
//     add_image_size ('portfolio-thumb', 374, 260, array('center', 'center'));

//     add_theme_support('automatic-feed-links');

//     register_nav_menus( array(
//         'primary'   => __( 'Primary Menu', 'mentor' )
//     ) );
    
// };
// add_action('after_setup_theme', 'mentor_theme_setup');


function mentor_scripts(){
   wp_enqueue_style('style',get_stylesheet_uri(  ));

   wp_enqueue_script( 'jquery');

   wp_enqueue_script( 'mentor_app',get_template_directory_uri(  ).'/assets/js/app.js');

}
add_action('wp_enqueue_scripts','mentor_scripts');
