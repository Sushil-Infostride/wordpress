<?php add_action( 'init', 'mentor_event' );

function mentor_event() {
    $args = array(
        'public' => true,
        'query_var' => 'event',
        'rewrite' => array(
            'slug' => 'events',
            'with_front' => false
        ),
        'supports' => array(
            'title',
            'thumbnail', 
            'editor',   
        ),
        'labels' => array(
            'name' => 'Events',
            'singular_name' => 'Events',
            'add_new' => 'Add New  Events',
            'add_new_item' => 'Add New  Events',
            'edit_item' => 'Edit Team Events',
            'new_item' => 'New  Events',
            'view_item' => 'View Events',
            'search_items' => 'Search Events',
            'not_found' => 'No event found',
            'not_found_in_trash' => 'No event found in Trash',
        ),
    );
    register_post_type( 'event', $args );
}?>