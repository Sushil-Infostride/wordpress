<?php add_action( 'init', 'mentor_count' );

function mentor_count() {
    $args = array(
        'public' => true,
        'query_var' => 'count',
        'rewrite' => array(
            'slug' => 'counts',
            'with_front' => false
        ),
        'supports' => array(
            'title',  
        ),
        'labels' => array(
            'name' => 'Counts',
            'singular_name' => 'Counts',
            'add_new' => 'Add New counts',
            'add_new_item' => 'Add New Team counts',
            'edit_item' => 'Edit Team counts',
            'new_item' => 'New Team counts',
            'view_item' => 'View counts',
            'search_items' => 'Search counts',
            'not_found' => 'No Membor found',
            'not_found_in_trash' => 'No Membor found in Trash',
        ),
    );
    register_post_type( 'count', $args );
}?>