<?php add_action( 'init', 'html2wp_product' );

function html2wp_product() {
    $args = array(
        'public' => true,
        'query_var' => 'product',
        'rewrite' => array(
            'slug' => 'products',
            'with_front' => false
        ),
        'supports' => array(
            'title',
            'thumbnail', 
            'editor',   
        ),
        'labels' => array(
            'name' => 'Products',
            'singular_name' => 'Products',
            'add_new' => 'Add New Team Products',
            'add_new_item' => 'Add New Team Products',
            'edit_item' => 'Edit Team Products',
            'new_item' => 'New Team Products',
            'view_item' => 'View Products',
            'search_items' => 'Search Products',
            'not_found' => 'No Membor found',
            'not_found_in_trash' => 'No Membor found in Trash',
        ),
    );
    register_post_type( 'product', $args );
}?>