<?php add_action( 'init', 'mentor_testimonial' );

function mentor_testimonial() {
    $args = array(
        'public' => true,
        'query_var' => 'testimonial',
        'rewrite' => array(
            'slug' => 'testimonials',
            'with_front' => false
        ),
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'revisions',
            
        ),
        'labels' => array(
            'name' => 'testimonials',
            'singular_name' => 'testimonial',
            'add_new' => 'Add New testimonial Member',
            'add_new_item' => 'Add New testimonial member',
            'edit_item' => 'Edit testimonial Member',
            'new_item' => 'New testimonial Member',
            'view_item' => 'View Member',
            'search_items' => 'Search Member',
            'not_found' => 'No Membor found',
            'not_found_in_trash' => 'No Membor found in Trash',
        ),
    );
    register_post_type( 'testimonial', $args );
}?>