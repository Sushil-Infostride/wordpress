<?php add_action( 'init', 'html2wp_team' );

function html2wp_team() {
    $args = array(
        'public' => true,
        'query_var' => 'team',
        'rewrite' => array(
            'slug' => 'teams',
            'with_front' => false
        ),
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'revisions',
            
        ),
        'labels' => array(
            'name' => 'Teams',
            'singular_name' => 'Team',
            'add_new' => 'Add New Team Member',
            'add_new_item' => 'Add New Team member',
            'edit_item' => 'Edit Team Member',
            'new_item' => 'New Team Member',
            'view_item' => 'View Member',
            'search_items' => 'Search Member',
            'not_found' => 'No Membor found',
            'not_found_in_trash' => 'No Membor found in Trash',
        ),
    );
    register_post_type( 'team', $args );
}?>