<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'youtube' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Lg88cG,weJ*C!odwQ?:lkKg~gBU`&Fqk!js.BjYHrF8ug6V?m2muo4`qY#icMDzA' );
define( 'SECURE_AUTH_KEY',  ' _!f3CMyYw,,}#)q4LfR2I.zI6MAaCU(m|.NQa$d=p9_E:{]>/2ifV]VgI)hqPw:' );
define( 'LOGGED_IN_KEY',    'h(m#uv.OYQB)DQVe^3>b:5@UU~3$Zl:`1Ki7E *qy{Ia7^*}$UCY:^avzT?d@0Qg' );
define( 'NONCE_KEY',        'MNhWl71_=WmU_:F4y7]LEsXIybmP_{a _f0c)0)).n=Tj{6-I1;U{:kU#WlPFnOC' );
define( 'AUTH_SALT',        '4h?U6o;hi(:wPD6^^Rao`E+srG!J?|g]CCfYtO&0;~wk~+c!mlh054NQWUL]Dgkk' );
define( 'SECURE_AUTH_SALT', '`lk-QmL%Mv#{iEb$#.v1=;!H5lgYN(T3.Mlp@a|(!D:3I%+=&c@h%rr16`!SXo,n' );
define( 'LOGGED_IN_SALT',   'jF73-JE+M48C#n2L<~%ql+SSqK>A$EF^l}I.1zE/B(9_HN(+Fu~:7qNn]rje=>@k' );
define( 'NONCE_SALT',       '<A4ZMe=ot-cQ6xXO-j`e0LA{mm[LXOr@n_ER|DGKZ(In/Hq[b1T4ET4x!I}v=GY_' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
